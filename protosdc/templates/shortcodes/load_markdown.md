{% set data = load_data(url=path, format="plain", required=true) -%}

{% set split_data = data | split(pat="\n") -%}
{% set_global currently_mermaid = false -%}

{% for line in split_data -%}
    {% if line is starting_with("```mermaid") %}
        {%- set_global currently_mermaid = true %}
<pre class="mermaid-pre">
<div class="mermaid">
    {%- elif line is starting_with("```") and currently_mermaid %}
        {%- set_global currently_mermaid = false -%}
</div>
</pre>
    {% elif currently_mermaid -%}
{{ line | safe | trim }}
{% else %}
{{ line | safe }}
    {% endif -%}
{%- endfor %}