+++
title = "Imprint"
description = "protoSDC Imprint"
date = 2021-05-01T08:00:00+00:00
updated = 2020-05-01T08:00:00+00:00
draft = false

[extra]
class = "page single"
+++

protoSDC.org is a community engagement advanced by Lukas Deichmann and David Gregorczyk with the core purpose to foster an easy to use and efficient foundation towards medical device interoperability.

We do not use cookies and we do not collect any personal data.

## Contact

E-Mail: &lt;first-name&gt;@protosdc.org

## Disclaimer

### 1. Content

The author reserves the right not to be responsible for the topicality, correctness, completeness or quality of the information provided. Liability claims regarding damage caused by the use of any information provided, including any kind of information which is incomplete or incorrect,will therefore be rejected.

All offers are not-binding and without obligation. Parts of the pages or the complete publication including all offers and information might be extended, changed or partly or completely deleted by the author without separate announcement.

### 2. Referrals and links

The author is not responsible for any contents linked or referred to from his pages - unless he has full knowledge of illegal contents and would be able to prevent the visitors of his site fromviewing those pages. If any damage occurs by the use of information presented there, only the author of the respective pages might be liable, not the one who has linked to these pages. Furthermore the author is not liable for any postings or messages published by users of discussion boards, guestbooks or mailinglists provided on his page.

### 3. Copyright

The author intended not to use any copyrighted material for the publication or, if not possible, to indicate the copyright of the respective object.
The copyright for any material created by the author is reserved. Any duplication or use of objects such as images, diagrams, sounds or texts in other electronic or printed publications is not permitted without the author's agreement.

### 4. Clinical use

The content provided by this website is not intended to be used in clinical trials, clinical studies, or in clinical routine.