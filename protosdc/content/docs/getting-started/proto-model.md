+++
title = "Proto Model"
description = "Model generator for protobuf messages and gRPC services."
date = 2021-12-15T08:00:00+00:00
updated = 2021-12-15T08:00:00+00:00
draft = false
weight = 50
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Proto Model uses Proto Converter to read the BICEPS participant and message model XML Schema and convert it accordingly. Proto Model also specifies the BICEPS service interface as well as a discovery/metadata model and service interface. All protobuf code fragments are compiled with protoc and deployed to the Gitlab Package Registry." 
toc = true
top = false
+++

The Proto Model documentation is hosted on <a href="https://model.protosdc.org" target="_blank">model.protosdc.org →</a>