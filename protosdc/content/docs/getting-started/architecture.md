+++
title = "Architecture"
description = "General approach of protoSDC"
date = 2021-12-15T08:00:00+00:00
updated = 2021-12-15T08:00:00+00:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "protoSDC is based on a tool suite that is capable of transforming XML Schemas to protobuf message types and generating pre-compiled packages to be used with different programming languages. The figure below shows the main building blocks of protoSDC and their interaction with each other." 
toc = true
top = false
+++

<img src="/images/protosdc_architecture.drawio_dark.svg" class="img-fluid img-dark" alt="protoSDC architecture"/>
<img src="/images/protosdc_architecture.drawio.svg" class="img-fluid img" alt="protoSDC architecture"/>

Essentially, there are two tools involved:

## Proto Converter

Proto Converter transforms XML Schemas to protobuf message types and other target languages (like for example Kotlin, Rust, or Python code). The tool binary is deployed to the Gitlab Package Registry.

<a href="https://converter.protosdc.org" target="_blank">Proto Converter Documentation →</a>

## Proto Model

Proto Model uses Proto Converter to read the BICEPS participant and message model XML Schema and convert it accordingly. Proto Model also specifies the BICEPS service interface as well as a discovery/metadata model and service interface. All protobuf code fragments are compiled with protoc and deployed to the Gitlab Package Registry.

<a href="https://model.protosdc.org" target="_blank">Proto Model Documentation →</a>