+++
title = "Proto Converter"
description = "Conversion of XML Schema to protobuf messages."
date = 2021-12-15T08:00:00+00:00
updated = 2021-12-15T08:00:00+00:00
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Proto Converter transforms XML Schemas to protobuf message types and other target languages (like for example Kotlin, Rust, or Python code). The tool binary is deployed to the Gitlab Package Registry." 
toc = true
top = false
+++

The Proto Converter documentation is hosted on <a href="https://converter.protosdc.org" target="_blank">converter.protosdc.org →</a>