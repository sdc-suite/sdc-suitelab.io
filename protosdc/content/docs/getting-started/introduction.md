+++
title = "Introduction"
description = "protoSDC introduction"
date = 2021-12-15T08:00:00+00:00
updated = 2021-12-15T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
# lead = 'protoSDC is a yada yada for modern connectivity, see <a href="https://grpc.io">gRPC</a>.'
toc = true
top = false
+++

Being a part of the IEEE 11073 SDC standards series, the Medical Devices Profile for Web Services aka MDPWS is used to be the vehicle to convey data between medical devices. However, MDPWS is based on stale standards like HTTP 1.1, Web Services and most prominently XML. This results in a variety of different challenges including but not limited to:

- The number of standards that are normatively enclosed by MDPWS exceeds three digits, which makes it nearly impossible to conduct verification measures at reasonable costs
- SDC is supposed to be run on resource-constrained medical devices, which considerably conflicts with XML being the default serialization technology of MDPWS (specifications for compact XML representations exist but are complicated to implement and hardly common)
- Unlike newer versions of HTTP, HTTP 1.1 does not provide bi-directional streaming of data, which makes it impossible for MDPWS to provide publish-subscribe style-like connections without fireing up HTTP servers on both the data provider and consumer sides

These challenges were the driving force to try out something new, i.e. coming up with an approach that references less standards, provides efficient serialization and implements publish-subscribe capabilities off the shelf. The result is protoSDC which is based on gRPC, a modern open source high performance Remote Procedure Call (RPC) framework that leverages protobuf, Google's language-neutral, platform-neutral, and extensible answer to XML. protoSDC provides a full-fleged transport layer definition for the Basic Integrated Clinical Environment Protocol Specification (BICEPS), comprising

- Conversion of XML Schema to protobuf message structures based on an XML Schema independent tree representation (which additionally allows for third parties to map back and forth between different representations, e.g. protobuf to Kotlin data classes)
- BICEPS services modelled as gRPC services
- Request-response and notification message exchange patterns
- Implicit and explicit endpoint discovery based on either ad-hoc (multicast) or managed (discovery proxy) mode

_Enough rambling_: Give it a try and find out how protoSDC can support you on your way to successful medical device interoperability.

## Quick Start

Summary of how to get started with protoSDC. [Quick Start →](../quick-start/)

## Go further

Contributing and Help.

### Contributing

👉 We need you! protoSDC is an open source initiative and relies on support by the community.  [Contributing →](/contributing/how-to-contribute/)

### Help

> Believe me, nothing is trivial.  
> [Brandon Lee]

You need support on your first steps with protoSDC? Get help by contacting us! [Imprint →](/imprint)
