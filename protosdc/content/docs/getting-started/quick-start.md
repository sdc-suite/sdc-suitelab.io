+++
title = "Quick Start"
description = "One page summary of how protoSDC is very quick to start."
date = 2021-12-15T08:00:00+00:00
updated = 2021-12-15T08:00:00+00:00
draft = false
weight = 20
sort_by = "weight"
template = "docs/page.html"

[extra]
# lead = "One page summary of how protoSDC is very quick to start."
toc = true
top = false
+++

Before being quick, you need to read this lame joke:

> Why did the noodle get voted out by his friends? Because everyone thought he was the impasta.

## At first...

... get yourself familiar with [Protocol Buffers](https://developers.google.com/protocol-buffers/docs/tutorials) and [gRPC](https://grpc.io/). Once you feel confident, either pull in a pre-compiled package or checkout the [Proto Model](https://gitlab.com/sdc-suite/proto-model) and assemble to your needs.

## Quick start using package management

Pre-compiled packages are available from the [Gitlab Package Registry](https://gitlab.com/sdc-suite/proto-model/-/packages?type=&orderBy=created_at&sort=desc&search[]=) for the following programming languages:

- Java
- Kotlin (JVM)
- Python

Additionally, output is generated for Rust.

### Java

Go to the [Gitlab Package Registry (Maven)](https://gitlab.com/sdc-suite/proto-model/-/packages?type=Maven&orderBy=created_at&sort=desc&search[]=) and select `protosdc-model-java` in a suitable version. Gitlab provides you with all information that is needed to include the library in your application. `protosdc-model-java` comprises protobuf model classes and gRPC services.

### Kotlin

Go to the [Gitlab Package Registry (Maven)](https://gitlab.com/sdc-suite/proto-model/-/packages?type=Maven&orderBy=created_at&sort=desc&search[]=) which offers you to pick from the following packages to start with your Kotlin application:

- `protosdc-model-kt`: compiled protobuf classes and gRPC services
- `biceps-model-kt`: Kotlin data class powered BICEPS model independent of protobuf
- `proto-kotlin-mapper`: API to map between the protobuf and Kotlin model classes

Gitlab provides you with all information that is needed to include the libraries in your application.


### Python

Go to the [Gitlab Package Registry (PyPI)](https://gitlab.com/sdc-suite/proto-model/-/packages/?type=PyPI&orderBy=created_at&sort=desc&search[]=) and select `proto-model` in a suitable version. Gitlab provides you with all information that is needed to include the library in your application. `proto-model` comprises protobuf model classes and gRPC services.


### Rust

While the `proto-model` is currently also being build for Rust, the resulting crates are not being deployed to a registry. Gitlab Package Registry [currently lacks the support](https://gitlab.com/gitlab-org/gitlab/-/issues/33060). This will change in the future by deploying the crates to the official package registry at [crates.io](https://crates.io/). For now, it is recommended to utilize a git submodule for Rust projects integrating protoSDC. This provides the following crates:

- `protosdc-proto`: contains the pre-generated protobuf and gRPC data, utilizing [tonic](https://github.com/hyperium/tonic) and [prost](https://github.com/tokio-rs/prost).
- `protosdc-biceps`: Rust structs representing the BICEPS model for an indepentent internal representation of data
- `protosdc-mappers`: API to map between the protobuf transport representation and the internal representation model



## Quick start checking out the Proto Model project

This section describes the manual checkout and invocation of the [Proto Model](https://gitlab.com/sdc-suite/proto-model) generation tool. Running the Proto Model tool manually should only be required if there is no pre-compiled package available that supports your preferred programming language.

If this is the case, please consider to contribute by adding your programming language to the Proto Model ecosystem. [Contributing →](/contributing/how-to-contribute)

---

**WARNING**

Building the proto model on **Microsoft Windows** is currently not officially supported as the build script makes use of symbolic links which cannot be processed properly by Windows. Solutions to enable the required support exist, but are outside of the scope of protoSDC.

Consider using the [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install) or installing a Virtual Machine and running a Unix-based operating system virtually to get Proto Model operating correctly. Proto Model on Windows has successfully been tested with [Oracle VM Box](https://www.virtualbox.org/) and [Manjaro](https://manjaro.org/). A tutorial is availabe at [linuxhint.com](https://linuxhint.com/install_manjaro_linux_virtualbox/).

---

### Step-by-step model generation

#### 1. Clone the repository

```bash
$ git clone https://gitlab.com/sdc-suite/proto-model.git
```

#### 2. Run the Proto Converter

```bash
$ cd proto-model
$ ./gradlew executeConverter
```

This executes the Proto Converter with the BICEPS participant and message model as input. The Proto Converter creates the following output folders:

- `proto_out`: includes BICEPS as protobuf message types
- `kotlin_out`: includes BICEPS as Kotlin data classes
- `proto_kotlin_out`: includes mapping between protobuf and Kotlin data classes
- `rust_out`: includes BICEPS as Rust structs
- `proto_rust_out`: includes mapping between protobuf and Rust structs

#### 3. Run protoc

After the model conversion has been performed, the folder `proto` includes the full protoSDC model consisting of

- the BICEPS participant and message model as protobuf message types (via a symbolic link),
- discovery and metadata protobuf message types, and
- BICEPS, discovery, and metadata service definitions.

It is now ready to be compiled with `protoc`. For example, you can compile to Java by running

```bash
$ cd proto
$ mkdir java_classes
$ protoc --java_out=./java_classes \
         --proto_path=./ \
		 $(find -L . -iname "*.proto")
```

This generates protobuf Java classes into `java_classes`. In order to generate gRPC Java classes into the same folder, first make sure you have the gRPC plugin for protoc in place. The following call then starts the code generation:

```bash
$ protoc --plugin=protoc-gen-grpc-java=/path/to/grpc-java-executable \
         --grpc-java_out=./java_classes \
		 --proto_path=./ \
		 $(find -L . -iname "*.proto")
```

