+++
title = "Modern Documentation Theme"

# The homepage contents
[extra]
title_img = "images/protosdc_logo_dark.svg"
title_img_dark = "images/protosdc_logo.svg"
lead = '<b>protoSDC</b> is a modern implementation of IEEE-11073 SDC via <a href="https://grpc.io/">gRPC</a> and <a href="https://developers.google.com/protocol-buffers">protobuf</a>.'
url = "/docs/getting-started/introduction/"
url_button = "Get started"
repo_version = "Current Version"
repo_license = "MIT License."
repo_url = "https://gitlab.com/sdc-suite/proto-model"

[[extra.list]]
title = "gRPC ⚙️"
content = 'gRPC is a modern open source high performance Remote Procedure Call (RPC) framework that can run in any environment.<p/><a href="https://grpc.io/" target="_blank">Learn more</a>'

[[extra.list]]
title = "protobuf ⚡️"
content = "Protocol buffers are Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data – think XML, but smaller, faster, and simpler.<p/><a href=\"https://developers.google.com/protocol-buffers\" target=\"_blank\">Learn more</a>"

[[extra.list]]
title = "IEEE 11073 🏥"
content = "CEN ISO/IEEE 11073 Health informatics - Medical / health device communication - a family of standards that enable communication between medical, health care and wellness devices and external computer systems.<p/><a href=\"https://ornet.org/en/services-2-2/\" target=\"_blank\">Learn more</a>"

+++
